package facci.herreraalexandra.gestiondefacturas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import facci.herreraalexandra.gestiondefacturas.ui.login.LoginActivity;

public class SplashActivity extends AppCompatActivity {

    static int TIMEOUT_MILLIIS = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        //Quitamos la action bar de la panatalla Splash.
        getSupportActionBar().hide();
        //La clase Handler permite enviar objetos de tipo Runnable.
        //Un objeto de tipo runnable da el significado a un hilo de proceso
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Intent es la definicion anstracta de una operacion a realizar.}
                //Puede ser usado para una activity, broadcast receiver, servicios
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                //Finaliza esta actividad
                finish();
            }
        }, TIMEOUT_MILLIIS);
    }
}
